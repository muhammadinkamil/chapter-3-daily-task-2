// bilangan 1 merupakan angka yang akan dipangkatkan
module.exports = function hasilPrintBilangan1(bilangan2) {
  // variabel baru
  const a = parseInt(bilangan2);
  // pengkondisioan
  if (isNaN(a)) {
    return false;
  } else {
    // menggunakan fungsi pasrseInt agar data yang diinputkan bertipe number
    return "bilangan pertama : " + a;
  }
};
