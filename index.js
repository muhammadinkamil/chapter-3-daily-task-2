var readline = require("readline");
var pertambahan = require("./pertambahan.js");
var pengurangan = require("./pengurangan.js");
const hasilPerkalian = require("./heri");
const printPangkat = require("./perpangkatan/bilangan1.js");
const printPangkat2 = require("./perpangkatan/bilangan2.js");
const hasilPembagian = require("./pembagian.js");
let rl = readline.createInterface(process.stdin, process.stdout);
//define function ngejalanin semua fungsi berdasarkan pilihan
function readyGo(param) {
  //kondisi if bersarang dengan else if dan else diujung akhir untuk ngecek input
  if (param == 1) {
    //pertambahan
    rl.question("Silakan masukan angka yang hendak ditambahkan, (contoh: 1,2,3) :", (answer1) => {
      console.log(pertambahan(answer1, rl));
      rl.close();
    });
  } else if (param == 2) {
    //pengurangan
    rl.question("Silakan masukan angka yang hendak dikurangkan, (contoh: 1-2) :", (answer2) => {
      console.log(pengurangan(answer2));
      rl.close();
    });
  } else if (param == 3) {
    //perkalian
    rl.question("ingin mengalikan angka berapa? ", (jawabanpertama) => {
      rl.question("dikalikan dengan angka berapa?  ", (jawabankedua) => {
        const angkaPerkalian = hasilPerkalian(parseInt(jawabanpertama), parseInt(jawabankedua));
        console.log("hasilnya = " + angkaPerkalian);
        rl.close();
      }
    );
  } else if (param == 4) {
    //perpangkatan
    rl.question("Masukkan bilangan pertama : ", (bilangan1) => {
      const hasilPrintBilangan = printPangkat(bilangan1);
      // kondisional bilangan1
      if (hasilPrintBilangan === false) {
        console.log("Masukkan Angka dan ulang kembali program!!");
        return rl.close();
      }
      console.log(hasilPrintBilangan);
      rl.question("Masukkan bilangan kedua : ", (bilangan2) => {
        const hasilPrintBilangan2 = printPangkat2(bilangan2);
        // kondisional bilangan2
        if (hasilPrintBilangan === false) {
          console.log("Masukkan Angka dan ulang kembali program!!");
          return rl.close();
        }
        console.log(hasilPrintBilangan2);
        const jumlah = Math.pow(bilangan1, bilangan2);
        console.log(bilangan1 + " pangkat " + bilangan2 + " = " + jumlah);
        rl.close();
      });
    });
  } else if (param == 5) {
    //pemnbagian
    rl.question(
      "Silakan masukan angka yang akan dibagi, (contoh: 4/2) :",
      (answer5) => {
        console.log(pembagian(answer5));
        rl.close();
      }
    );
  } else {
    //jika user input selain angka, outputnya seperti berikut
    console.log("Maaf, input yang anda masukan salah, jalankan program lagi");
    rl.close();
  }
}

//eksekusi program utama
rl.question("Silakan pilih angka operasi aritkmatika yang dikehendaki (contoh: 1) :\n1. Pertambahan \n2.Pengurangan \n3. Perkalian \n4.Perpangkatan \n5.Pembagian \nPilih Angka :", (answer) => {
  //invoke function ngejalanin semua fungsi berdasarkan pilihan yang diinput ke param answer
  readyGo(answer);
});

/* Daily Task 1
rl.question(
  "Silakan masukan angka yang hendak ditambahkan, (contoh: 1,2,3) :",
  (answer1) => {
    pertambahan(answer1);
    rl.question(
      "Silakan masukan angka yang hendak dikurangkankan lengkap dengan operator - ",
      (answer2) => {
        let hasil = pengurangan(answer2);
        console.log(hasil);
        rl.question("ingin mengalikan angka berapa? ", (jawabanpertama) => {
          rl.question("dikalikan dengan angka berapa?  ", (jawabankedua) => {
            const angkaPerkalian = hasilPerkalian(
              parseInt(jawabanpertama),
              parseInt(jawabankedua)
            );
            console.log("hasilnya = " + angkaPerkalian);
            rl.question("Masukkan bilangan pertama : ", (bilangan1) => {
              const hasilPrintBilangan1 = printPangkat(bilangan1);
              console.log(hasilPrintBilangan1);

              rl.question("Masukkan bilangan kedua : ", (bilangan2) => {
                const hasilPrintBilangan2 = printPangkat2(bilangan2);
                console.log(hasilPrintBilangan2);

                // Perhitungan pangkat menggunakan fungsi Math.pow
                const jumlah = Math.pow(bilangan1, bilangan2);
                console.log(
                  bilangan1 + " pangkat " + bilangan2 + " = " + jumlah
                );
                rl.question("ingin membagi angka berapa? ",(jawabanpertama) => {
                    rl.question("dibagi dengan angka berapa?  ",(jawabankedua) => {
                        const angkaPembagian = hasilPembagian(
                          parseInt(jawabanpertama),
                          parseInt(jawabankedua)
                        );
                        console.log("hasilnya = " + angkaPembagian);
                        rl.close();
                      }
                    );
                  }
                );
              });
            });
          });
        });
      }
    );
  }
);
*/
